#include <stdio.h>
#include <ncurses.h>
#include <alpm.h>

int search(int argc, char* argv[])
{
    initscr();
    cbreak();
    noecho();
    while(argc--) {
        printw("Search: %s\n", argv[argc]);
    }
    while(1) {
        int c = getch();
        switch(c) {
            case 27:
                refresh();
                endwin();
                return 0;
            default:
                printw("Got %i\n\n", c);
        }
    }
}

int main(int argc, char* argv[])
{
    if (argc == 1) {
        printf("Please provide a search term\n");
        return 1;
    }
    search(argc - 1, ++argv);
}
